# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Henrik Blidh
# Copyright (c) 2022 The Pybricks Authors

import asyncio
from contextlib import asynccontextmanager
from typing import AsyncIterator, Iterator
from bleak import AdvertisementData, BleakScanner, BleakClient, BLEDevice, BleakGATTCharacteristic

from train.controller import NapTrainController


UART_SERVICE_UUID = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
UART_RX_CHAR_UUID = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
UART_TX_CHAR_UUID = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

# Replace this with the name of your hub if you changed it when installing the Pybricks firmware.
HUB_NAME = "Tchoutchou"


def hub_filter(device: BLEDevice, ad: AdvertisementData) -> bool:
    return device.name and device.name.lower() == HUB_NAME.lower()


def handle_disconnect(_) -> None:
    print("Hub was disconnected.")


def handle_rx(_, data: bytearray) -> None:
    print("Received:", data)


@asynccontextmanager
async def connect_to_train() -> AsyncIterator[NapTrainController]:
    # Find the device and initialize client.
    device = await BleakScanner.find_device_by_filter(hub_filter)
    client = BleakClient(device, disconnected_callback=handle_disconnect)

    await client.connect()
    try:
        # Connect and get services.
        await client.start_notify(UART_TX_CHAR_UUID, handle_rx)
        nus = client.services.get_service(UART_SERVICE_UUID)
        rx_char = nus.get_characteristic(UART_RX_CHAR_UUID)

        # Tell user to start program on the hub.
        print("Start the program on the hub now with the button.")

        yield NapTrainController(client, rx_char)
    except Exception as e:
        print(e)
    finally:
        await asyncio.sleep(1)
        await client.disconnect()


async def main():
    async with connect_to_train() as train:
        print("go")
        await train.go()
        await asyncio.sleep(60)
        print("stop")
        await train.stop()


if __name__ == "__main__":
        asyncio.run(main())
