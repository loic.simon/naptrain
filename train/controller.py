from bleak import BleakClient, BleakGATTCharacteristic


def log(func):
    async def wrapper(*args, **kwargs):
        print(f"Calling {func.__name__} with {args} and {kwargs}")
        return await func(*args, **kwargs)

    return wrapper

class NapTrainController:
    def __init__(self, client: BleakClient, characteristic: BleakGATTCharacteristic) -> None:
        self.client = client
        self.characteristic = characteristic

    @log
    async def go(self, speed: int = 50) -> None:
        print(f"Send go cmd, {speed}")
        await self._send(f"fwd,{speed}".encode())
    
    @log
    async def stop(self) -> None:
        print("Send stop cmd")
        await self._send(b"stop")

    @log
    async def light_on(self) -> None:
        await self._send(b"l on")

    @log
    async def light_off(self) -> None:
        await self._send(b"l off")

    @log
    async def light_wave(self) -> None:
        await self._send(b"l wave")

    async def _send(self, data: bytes) -> None:
        if len(data) > 8:
            raise ValueError("Cannot send more than 8 chars to the train!")
        await self.client.write_gatt_char(self.characteristic, data.ljust(8))
