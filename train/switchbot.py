import asyncio
import enum

from bleak import AdvertisementData, BleakClient, BleakScanner, BLEDevice


# SwitchBot user-defined service, read characteristic, and notify characteristic
UART_SERVICE_UUID = "cba20d00-224d-11e6-9fb8-0002a5d5c51b"
UART_RX_CHAR_UUID = "cba20002-224d-11e6-9fb8-0002a5d5c51b"
UART_TX_CHAR_UUID = "cba20003-224d-11e6-9fb8-0002a5d5c51b"


class BotCommand(enum.Enum):
    PRESS = b"\x57\x01\x00"
    ON = b"\x57\x01\x01"
    OFF = b"\x57\x01\x02"
    OPEN = b"\x57\x0F\x45\x01\x05\xFF\x00"
    CLOSE = b"\x57\x0F\x45\x01\x05\xFF\x64"
    PAUSE = b"\x57\x0F\x45\x01\x00\xFF"


# SWITCHBOT_MAC = "d058cb71f3e4"
SWITCHBOT_MAC = "eb1fd2237b2e"


def hub_filter(device: BLEDevice, ad: AdvertisementData) -> bool:
    for value in ad.manufacturer_data.values():
        if value.hex().strip().lower() == SWITCHBOT_MAC:
            return True
    return False


async def press_button():
    device = await BleakScanner.find_device_by_filter(hub_filter)
    client = BleakClient(device, disconnected_callback=lambda x: print("Hub was disconnected."))

    await client.connect()
    try:
        print("Connected:     ", client)
        service = client.services.get_service(UART_SERVICE_UUID)
        print("Service:       ", service)
        chr = service.get_characteristic(UART_RX_CHAR_UUID)
        print("Characteristic:", (chr.handle, chr.uuid, chr.descriptors, chr.properties))

        await client.write_gatt_char(chr, BotCommand.PRESS.value)
        await asyncio.sleep(3)

    except Exception as e:
        print(e)
    finally:
        await client.disconnect()


if __name__ == "__main__":
    asyncio.run(press_button())
