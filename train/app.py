import asyncio

from fastapi import FastAPI

from train.controller import NapTrainController
from train.switchbot import press_button
from train.train_handler import connect_to_train

async def train_cmd(train: NapTrainController):
    await train.go(80)
    await asyncio.sleep(15)
    await train.stop()


async def main():
    await press_button()
    async with connect_to_train() as train:
        await press_button()
        await train_cmd(train)

app = FastAPI()

@app.get("/ping")
async def ping() -> str:
    return "pong"

@app.get("/run_train")
async def run_train() -> str:
    await main()
    return "OK"


if __name__ == "__main__":
        asyncio.run(main())
