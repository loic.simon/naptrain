# NOTE: Run this program with the latest firmware provided via https://beta.pybricks.com/

from pybricks.hubs import CityHub
from pybricks.pupdevices import DCMotor, Light
from pybricks.parameters import Port, Color
from pybricks.tools import wait, StopWatch

# Standard MicroPython modules
from umath import pi, cos
from usys import stdin, stdout
from uselect import poll

hub = CityHub()
motor = DCMotor(Port.B)
lights = Light(Port.A)

# Optional: Register stdin for polling. This allows you to wait for incoming data without blocking.
keyboard = poll()
keyboard.register(stdin)

def light_wave():
    watch = StopWatch()

    # Cosine pattern properties.
    PERIOD = 2000
    MAX = 100

    # Make the brightness fade in and out.
    while True:
        # Get phase of the cosine.
        phase = watch.time() / PERIOD * 2 * pi

        # Evaluate the brightness.
        brightness = (0.5 - 0.5 * cos(phase)) * MAX

        # Set light brightness and wait a bit.
        lights.on(brightness)
        wait(10)

while True:
    hub.light.on(Color.GREEN)
    wait(500)
    hub.light.off()

    cmd = stdin.buffer.read(8)
    rep = b"OK"

    command_splitted = cmd.strip().split(b",")
    command = command_splitted[0]
    if len(command_splitted) == 2:
        speed = command_splitted[1]
        if command == b"fwd" and speed.isdigit():
            motor.dc(int(speed))
        elif command == b"bck" and speed.isdigit():
            motor.dc(-int(speed))
        else:
            rep = "INVALID"
    else:
        if command == b"stop":
            motor.stop()
        elif command == b"l on":
            lights.on()
        elif command == b"l off":
            lights.off()
        elif command == b"l wave":
            light_wave()
        else:
            rep = "INVALID"

    # Send a response.
    stdout.buffer.write(rep)
